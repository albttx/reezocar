
BIN_NAME=reezocli

.PHONY: build
build:
	GOOS=linux GOARCH=amd64 go build -o $(BIN_NAME) ./cmd/reezocli

up:
	docker-compose up -d

clean:
	docker-compose down
	rm $(BIN_NAME)