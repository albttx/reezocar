package broker

import (
	"context"

	kafka "github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

type Broker interface {
	PubCarURL(msg ...string)
	SubCarURL() chan string
	Close() error
}

type Config struct {
	Host      string
	Topic     string
	Partition int
}

func NewBrokerKafka(cfg Config) (Broker, error) {
	b := &Kafka{}

	conn, err := kafka.DialLeader(context.Background(), "tcp", cfg.Host, cfg.Topic, cfg.Partition)
	if err != nil {
		return nil, err
	}

	b.conn = conn
	return b, nil
}

type Kafka struct {
	conn *kafka.Conn
}

func (k *Kafka) PubCarURL(messages ...string) {
	for _, msg := range messages {
		logrus.Info("kafka send:", msg)

		k.conn.WriteMessages(kafka.Message{
			Value: []byte(msg),
		})
	}
}

func (k *Kafka) SubCarURL() chan string {
	chSubCarURL := make(chan string)

	go func() {
		defer close(chSubCarURL)
		batch := k.conn.ReadBatch(1e3, 1e6) // fetch 10KB min, 1MB max
		defer batch.Close()

		for {
			b := make([]byte, 10e6) // 10KB max per message
			_, err := batch.Read(b)
			if err != nil {
				break
			}
			chSubCarURL <- string(b)
			// logrus.Infof("kafka recv: %s", b)
		}
	}()
	return chSubCarURL
}

func (k *Kafka) Close() error {
	return k.conn.Close()
}
