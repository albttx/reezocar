package car

type Car interface {
	GetName() (string, error)
	GetPrice() (float64, error)
	GetYear() (int, error)
	GetKilometers() (float64, error)
	GetLocation() (string, error)
	GetEngine() (string, error)
	GetGear() (string, error)
	GetPower() (string, error)

	// Options []string
}
