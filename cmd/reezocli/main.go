package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/ghodss/yaml"
	"github.com/sirupsen/logrus"
	cli "github.com/urfave/cli/v2"
)

var cfg = &config{}

type config struct {
	Kafka struct {
		Host string `json:"host"`
	} `json:"kafka"`
}

func main() {
	app := &cli.App{
		Name:  "reezocar scrapper",
		Usage: "reezocar websites scrapper",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "config",
				Value:   "config.yml",
				Aliases: []string{"c"},
			},
		},
		Before: func(c *cli.Context) error {
			file, err := ioutil.ReadFile(c.String("config"))
			if err != nil {
				return fmt.Errorf("Failed to read config file, %v", err)
			}
			if err := yaml.Unmarshal(file, &cfg); err != nil {
				return fmt.Errorf("Failed to parse config file, %v", err)
			}

			return nil
		},
		Commands: []*cli.Command{
			cliAutoreflex,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal()
	}
}
