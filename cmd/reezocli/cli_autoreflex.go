package main

import (
	"github.com/albttx/reezocar/pkg/broker"
	"github.com/albttx/reezocar/scraper/autoreflex"
	"github.com/sirupsen/logrus"
	cli "github.com/urfave/cli/v2"
)

var cliAutoreflex = &cli.Command{
	Name:  "autoreflex",
	Usage: "Autoreflex scrapper cli",
	Subcommands: []*cli.Command{
		{
			Name:  "pub",
			Usage: "Start autoreflex publisher",
			Action: func(c *cli.Context) error {
				broker, err := broker.NewBrokerKafka(broker.Config{
					Host:      cfg.Kafka.Host,
					Topic:     "autoreflex",
					Partition: 0,
				})
				if err != nil {
					return err
				}
				defer broker.Close()

				scrapper := autoreflex.NewScraper(autoreflex.Config{
					BrokerConn: broker,
				})

				logrus.Info("Start scrapping for autoreflex")
				scrapper.StartPub()
				return nil
			},
		},

		{
			Name:  "sub",
			Usage: "Start autoreflex subscriber",
			Action: func(c *cli.Context) error {
				broker, err := broker.NewBrokerKafka(broker.Config{
					Host:      cfg.Kafka.Host,
					Topic:     "autoreflex",
					Partition: 0,
				})
				if err != nil {
					return err
				}
				defer broker.Close()

				scrapper := autoreflex.NewScraper(autoreflex.Config{
					BrokerConn: broker,
				})

				logrus.Info("Start scrapping for autoreflex")
				scrapper.StartSub()
				return nil
			},
		},
	},
}
