module github.com/albttx/reezocar

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/antchfx/htmlquery v1.2.2 // indirect
	github.com/antchfx/xmlquery v1.2.3 // indirect
	github.com/antchfx/xpath v1.1.5 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/gocolly/colly v1.2.0
	github.com/gocolly/colly/v2 v2.0.1 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/segmentio/kafka-go v0.3.5
	github.com/sirupsen/logrus v1.5.0
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/tools v0.0.0-20200416185413-ea516885154d // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
