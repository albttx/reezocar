package autoreflex

import (
	"fmt"

	"github.com/albttx/reezocar/pkg/broker"
	"github.com/gocolly/colly"
	"github.com/sirupsen/logrus"
)

const WebsiteURL = "http://www.autoreflex.com/"

type Specs struct {
	Name       string
	Price      string
	Years      string
	Kilometers string
	Location   string
	Engine     string
	Gear       string
	Power      string

	Options []string
}

type Scraper struct {
	c *colly.Collector

	brokerConn broker.Broker
}

type Config struct {
	BrokerConn broker.Broker
}

func NewScraper(cfg Config) *Scraper {
	s := &Scraper{
		c:          colly.NewCollector(),
		brokerConn: cfg.BrokerConn,
	}
	// s.c
	return s
}

func (s *Scraper) StartPub() {
	s.c.OnHTML("tr a[href]", s.scrapList)

	s.c.OnRequest(func(r *colly.Request) {
		logrus.WithField("Visiting", r.URL).Info()
	})

	s.c.Visit("http://www.autoreflex.com/137.0.-1.-1.-1.0.999999.1900.999999.-1.99.0.1")
}

func (s *Scraper) StartSub() {
	// s.c.OnHTML("tr a[href]", s.scrapList)
	s.c.OnHTML("*", func(e *colly.HTMLElement) {
		fmt.Println(e)
	})

	s.c.OnHTML("section#page", s.scrapCarInfo)

	s.c.OnRequest(func(r *colly.Request) {
		logrus.WithField("Visiting", r.URL).Info()
	})

	s.c.OnError(func(r *colly.Response, err error) {
		fmt.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
	})

	// s.c.Visit(WebsiteURL)

	chSubCarURL := s.brokerConn.SubCarURL()
	for carURL := range chSubCarURL {
		s.c.Visit(carURL)
		logrus.Info(carURL)
	}
}
