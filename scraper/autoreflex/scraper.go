package autoreflex

import (
	"github.com/gocolly/colly"
	"github.com/sirupsen/logrus"
)

// scrapList scrap the list page on autoreflex
// must be OnHTML "tr a[href]"
func (s *Scraper) scrapList(e *colly.HTMLElement) {
	link := e.Attr("href")
	if link == "credit-auto-occasion.asp" {
		return
	}

	logrus.WithField("link", link).Debug()
	s.brokerConn.PubCarURL(link)
}

// scrapCarInfo scrap the page with car information
// must be OnHTML "section#page"
func (s *Scraper) scrapCarInfo(e *colly.HTMLElement) {
	carSpecs := &Specs{
		Name:  e.ChildText(".header h1"),
		Price: e.ChildText(".prix"),
	}

	// Parse top specs website information
	specsMap := map[string]*string{
		"icon- iconsearch-location": &carSpecs.Location,
		"icon- iconsearch-km":       &carSpecs.Kilometers,
		"icon- iconsearch-annee":    &carSpecs.Years,
		"icon- iconsearch-engine":   &carSpecs.Engine,
		"icon- iconsearch-gear":     &carSpecs.Gear,
		"icon- iconsearch-power":    &carSpecs.Power,
	}
	e.ForEach(".specs ul li", func(_ int, el *colly.HTMLElement) {
		classType := el.ChildAttr("i", "class")
		if ptr, ok := specsMap[classType]; ok {
			*ptr = el.Text
		}
	})

	e.ForEach(".equipements ul li", func(_ int, el *colly.HTMLElement) {
		carSpecs.Options = append(carSpecs.Options, el.Text)
	})

	logrus.Infof("specs : %#v", carSpecs)
}
